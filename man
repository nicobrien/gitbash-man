#!/bin/sh

# A workable man command for git-bash
# Tested on Windows 10, mintty 2.0.3, MINGW64

# This fetches from http://man.he.net so you need to be connected
# to the internet


# Command line parameter is required
if [ "$#" -ne 1 ]; then
    echo "Please specify a command to fetch man page for"
    exit 1
fi

# curl is used to fetch the specified resource,
# sed is used to trim anything matching html tags
# tail because the server sends some erroneous text at the start
curl -s -X GET "http://man.he.net/?topic=$1&section=all" \
    | sed 's/<[^>]\+>/ /g' \
    | tail -n +10 \
    | less
